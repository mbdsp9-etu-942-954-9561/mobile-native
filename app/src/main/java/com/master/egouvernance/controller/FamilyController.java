package com.master.egouvernance.controller;

import com.master.egouvernance.model.Membrefamille;
import com.master.egouvernance.service.FamilyService;
import com.master.egouvernance.utils.Enum;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FamilyController {
    private static FamilyController instance = null;

    FamilyService service;

    private FamilyController() {
        super();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Enum.Settings.BASE_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(FamilyService.class);
    }

    public static FamilyController getInstance() {
        if (instance == null) {
            instance = new FamilyController();
        }
        return instance;
    }

    /**
     * Take family member with the id of the member connected
     * @param citizenId
     * @return List of family's member
     */
    public Call<ArrayList<Membrefamille>> livretDeFamille(int citizenId){
        return service.livretDeFamille(citizenId);
    }

    /**
     * Get detail of a family's member
     * @param citizenId
     * @return VMembreFamille
     */


}
