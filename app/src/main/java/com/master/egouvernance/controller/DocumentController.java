package com.master.egouvernance.controller;

import com.master.egouvernance.model.DemandeDocument;
import com.master.egouvernance.model.TypeDocument;
import com.master.egouvernance.service.DocumentService;
import com.master.egouvernance.utils.Enum;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DocumentController {

    private static DocumentController instance = null;

    DocumentService service;

    private DocumentController() {
        super();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Enum.Settings.BASE_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(DocumentService.class);
    }

    public static DocumentController getInstance() {
        if (instance == null) {
            instance = new DocumentController();
        }
        return instance;
    }

    public Call<ArrayList<TypeDocument>> getDocuments(){
        return service.getDocuments();
    }

    public Call<DemandeDocument> createDocument(DemandeDocument document){
        return service.createDocument(document);
    }

}
