package com.master.egouvernance.controller;

import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.model.utils.Dto;
import com.master.egouvernance.service.SessionService;
import com.master.egouvernance.utils.Enum;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SessionController {

    private static SessionController instance = null;

    SessionService service;

    private SessionController() {
        super();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Enum.Settings.BASE_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(SessionService.class);
    }

    public static SessionController getInstance() {
        if (instance == null) {
            instance = new SessionController();
        }
        return instance;
    }

    /**
     * Connexion
     * @param username
     * @param password
     * @return membreId (member authenticate)
     */
    public Call<Citoyen> signIn(String username, String password){
        Dto.SignInDto signInDto = new Dto.SignInDto(username, password);
        return service.signIn(signInDto);
    }

    /**
     * Connexion via scan QRCode
     * @param qrcode
     * @return leadId (family leader)
     */
    public Call<Citoyen> scanQrCode(String qrcode){
        return service.scan(qrcode);
    }

    public Call<Citoyen> getCitoyen(int citizenId){
        return service.getCitoyen(citizenId);
    }

}
