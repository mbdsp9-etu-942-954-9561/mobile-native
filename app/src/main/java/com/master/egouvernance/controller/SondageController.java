package com.master.egouvernance.controller;

import com.master.egouvernance.model.Projet;
import com.master.egouvernance.model.Sondage;
import com.master.egouvernance.model.utils.Dto;
import com.master.egouvernance.service.SondageService;
import com.master.egouvernance.utils.Enum;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SondageController {
    private static SondageController instance = null;

    SondageService service;

    private SondageController() {
        super();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Enum.Settings.BASE_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(SondageService.class);
    }

    public static SondageController getInstance() {
        if (instance == null) {
            instance = new SondageController();
        }
        return instance;
    }

    /**
     * @return List of projects
     */
    public Call<ArrayList<Projet>> getProjets() {
        return service.getProjets();
    }

    /**
     * Take the project
     * @param id index of the project
     */
    public Call<Projet> getProjet(int id) {
        return service.getProjet(id);
    }

    /**
     * Get the result positive of the project
     * @param projetId
     * @return pourcentage
     */
    public Call<Double> getResult(int projetId){
        return service.getProjetResult(projetId);
    }

    /**
     * Search if the member connected can vote
     * @param citizenId
     * @return True or False
     */
    public Call<Dto.ProjetVote> canVote(int citizenId, int projetId){
        return service.canVote(citizenId, projetId);
    }

    public Call<Sondage> vote(Sondage sondage){
        return service.create(sondage);
    }
}
