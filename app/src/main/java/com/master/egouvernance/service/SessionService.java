package com.master.egouvernance.service;

import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.model.utils.Dto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SessionService {

    // Return familyId
    @POST("citoyen/login")
    Call<Citoyen> signIn(@Body Dto.SignInDto signInDto);

    @GET("famille/scan/{qrcode}")
    Call<Citoyen> scan(@Path("qrcode") String qrcode);

    @GET("citoyen/readById/{id}")
    Call<Citoyen> getCitoyen(@Path("id") int id);
}
