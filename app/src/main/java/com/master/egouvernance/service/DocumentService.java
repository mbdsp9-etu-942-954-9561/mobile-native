package com.master.egouvernance.service;

import com.master.egouvernance.model.DemandeDocument;
import com.master.egouvernance.model.TypeDocument;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface DocumentService {

    @POST("demandeDocument/create")
    Call<DemandeDocument> createDocument(@Body DemandeDocument document);

    /**
     * Get list of TypeDocument
     */
    @GET("typeDocument/read")
    Call<ArrayList<TypeDocument>> getDocuments();
}
