package com.master.egouvernance.service;

import com.master.egouvernance.model.Membrefamille;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FamilyService {

    @GET("membrefamille/livret/{citizenId}")
    Call<ArrayList<Membrefamille>> livretDeFamille(@Path("citizenId") int citizenId);



}
