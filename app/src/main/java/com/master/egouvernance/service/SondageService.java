package com.master.egouvernance.service;

import com.master.egouvernance.model.Projet;
import com.master.egouvernance.model.Sondage;
import com.master.egouvernance.model.utils.Dto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SondageService {

    @GET("projet/read")
    Call<ArrayList<Projet>> getProjets();

    @GET("projet/readById/{projetId}")
    Call<Projet> getProjet(@Path("projetId") int index);

    @GET("projet/getAvancement/{projetId}")
    Call<Double> getProjetResult(@Path("projetId") int index);

    @GET("sondage/projetVote/{citizenId}/{projetId}")
    Call<Dto.ProjetVote> canVote(@Path("citizenId") int citizenId, @Path("projetId") int projetId);

    @POST("sondage/create")
    Call<Sondage> create(@Body Sondage sondage);
}
