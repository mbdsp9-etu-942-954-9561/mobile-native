package com.master.egouvernance.utils;

public class Enum {

    public enum Settings {

        SHARED("shared_preferences"),

        CITIZEN("citizen"),
        CITIZEN_ID("member_id"),

        DOCUMENT_MESSAGE("Votre document sera prêt dans 48h"),

        BASE_URL("http://85.31.234.130");

        private final String value;

        Settings(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
