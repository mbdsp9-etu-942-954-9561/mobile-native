package com.master.egouvernance.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.master.egouvernance.R;
import com.master.egouvernance.view.component.HeaderFragment;

public class Tools {

    static Gson gson;

    /**
     * Add subtitle to a page
     * @param activity to add this part
     * @param aClass previous activity's class
     * @param title of page
     */
    public static void header(AppCompatActivity activity, @Nullable Class<?> aClass, int title ){

        HeaderFragment headerFragment = HeaderFragment.newInstance(aClass, activity.getString(title));
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, headerFragment);
        fragmentTransaction.commit();

    }

    public static Gson getGson(){
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }
}
