package com.master.egouvernance.model.utils;

import com.master.egouvernance.model.Projet;

public class Dto {
    public static class SignInDto {

        String idcard, password;

        public SignInDto(String idcard, String password) {
            this.idcard = idcard;
            this.password = password;
        }

        public String getIdcard() {
            return idcard;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class ProjetVote {
        Projet projet;
        boolean canVote;
        long totalVote;
        long forVote;

        public ProjetVote(Projet projet, boolean canVote, long totalVote, long forVote) {
            this.projet = projet;
            this.canVote = canVote;
            this.totalVote = totalVote;
            this.forVote = forVote;
        }


        public Projet getProjet() {
            return projet;
        }

        public boolean isCanVote() {
            return canVote;
        }

        public long getTotalVote() {
            return totalVote;
        }

        public long getForVote() {
            return forVote;
        }

        public void setProjet(Projet projet) {
            this.projet = projet;
        }

        public void setCanVote(boolean canVote) {
            this.canVote = canVote;
        }

        public void setTotalVote(long totalVote) {
            this.totalVote = totalVote;
        }

        public void setForVote(long forVote) {
            this.forVote = forVote;
        }
    }
}
