/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.master.egouvernance.model;

import java.util.Date;


public class Projet {

    private Integer id;

    private String title;

    private String _description;

    private Double budget;

    private Date startDate;

    private Date dueDate;

    public Projet() {
    }

    public Projet(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String get_description() {
        return _description;
    }

    public Double getBudget() {
        return budget;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }
}