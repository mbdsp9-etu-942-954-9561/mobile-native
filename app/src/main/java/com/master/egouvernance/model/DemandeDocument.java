package com.master.egouvernance.model;

import java.util.Date;

public class DemandeDocument {

    private Integer id;

    private Citoyen citizen;

    private TypeDocument document;

    Date requestDate;

    public DemandeDocument(Integer citizenId, TypeDocument typeDocument){
        citizen = new Citoyen(citizenId);
        document = typeDocument;
    }

    public Integer getId() {
        return id;
    }

    public Citoyen getCitizen() {
        return citizen;
    }

    public TypeDocument getDocument() {
        return document;
    }

    public Date getRequestDate() {
        return requestDate;
    }


}
