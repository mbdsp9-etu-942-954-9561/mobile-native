package com.master.egouvernance.model;

//import javax.persistence.*;

import java.util.Date;


public class Citoyen {

    private Integer id;

    private String name;

    private String firstname;

    private Date birthday;

    private String gender;

    private String idcard;

    private Date deathdate;

    private LifeStatus lifeStatus;

    private String password;

    public Citoyen() {
    }

    public Citoyen(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirstname() {
        return firstname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getIdcard() {
        return idcard;
    }

    public Date getDeathdate() {
        return deathdate;
    }

    public LifeStatus getLifeStatus() {
        return lifeStatus;
    }

    public String getPassword() {
        return password;
    }
}
