package com.master.egouvernance.model;

//import javax.persistence.*;

public class Famille {
	
	private Integer id;
	
	private Citoyen familylead;
	
	private String qrcode;

	public Integer getId() {
		return id;
	}

	public Citoyen getFamilylead() {
		return familylead;
	}

	public String getQrcode() {
		return qrcode;
	}
}
