/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.master.egouvernance.model;

public class Sondage {
    private Integer id;
    
    private Projet projet;
    
    private Citoyen citizen;
    
    private Integer choice;

    public Sondage(Integer projetId, Integer citizenId, Integer choice) {
        this.projet = new Projet(projetId);
        this.citizen = new Citoyen(citizenId);
        this.choice = choice;
    }

    public Integer getId() {
        return id;
    }

    public Projet getProjet() {
        return projet;
    }

    public Citoyen getCitizen() {
        return citizen;
    }

    public Integer getChoice() {
        return choice;
    }
}
