package com.master.egouvernance.model;

import java.util.Date;


public class Membrefamille {

    private Integer id;

    private Famille family;

    private Citoyen citizen;

    private Date lastupdate;

    public Integer getId() {
        return id;
    }

    public Famille getFamily() {
        return family;
    }

    public Citoyen getCitizen() {
        return citizen;
    }

    public Date getLastupdate() {
        return lastupdate;
    }
}
