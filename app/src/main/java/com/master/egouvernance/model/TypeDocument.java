package com.master.egouvernance.model;

public class TypeDocument {
	private Integer id;
	
	private String _description;

	public TypeDocument() {
	}

	public TypeDocument(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String get_description() {
		return _description;
	}
}
