package com.master.egouvernance.model;

public class LifeStatus {
    private Integer id;

    private String label;

    public Integer getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}
