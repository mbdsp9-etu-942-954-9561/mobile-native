package com.master.egouvernance.view.component;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.master.egouvernance.R;
import com.master.egouvernance.model.Membrefamille;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.view.MemberActivity;

import java.util.ArrayList;

public class FamilyAdapter extends RecyclerView.Adapter<FamilyAdapter.MyHolder> {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Membrefamille> membrefamilles;

    public FamilyAdapter(Context context, ArrayList<Membrefamille> membrefamilles) {
        this.context = context;
        this.membrefamilles = membrefamilles;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_family, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        Membrefamille membrefamille = this.membrefamilles.get(position);
        String name = membrefamille.getCitizen().getName() + " " + membrefamille.getCitizen().getFirstname();
        holder.txtBirthday.setText(membrefamille.getCitizen().getBirthday().toString());
        holder.txtName.setText(name);

        holder.cardMember.setOnClickListener(v->{

            Log.d("E-Gouvernance Message", "Membre lien ******************************************");

            SharedPreferences sharedPreferences = context.getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);
            // Save the id of the member to see
            sharedPreferences.edit().putInt(Enum.Settings.CITIZEN_ID.getValue(), membrefamille.getCitizen().getId()).apply();

            Intent intent = new Intent(context, MemberActivity.class);
            context.startActivity(intent);

        });

    }

    @Override
    public int getItemCount() {
        return this.membrefamilles.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtBirthday;
        CardView cardMember;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            cardMember = itemView.findViewById(R.id.cardMember);
            txtName = itemView.findViewById(R.id.txt_name);
            txtBirthday = itemView.findViewById(R.id.txt_birthday);
        }

    }
}
