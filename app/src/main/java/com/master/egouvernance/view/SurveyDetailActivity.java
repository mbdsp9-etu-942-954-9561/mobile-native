package com.master.egouvernance.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.SondageController;
import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.model.Projet;
import com.master.egouvernance.model.Sondage;
import com.master.egouvernance.model.utils.Dto;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyDetailActivity extends AppCompatActivity {

    private SondageController instance;
    private Projet projet;
    CardView cardResult;
    private int projetId;
    private Citoyen citizen;
    SharedPreferences sharedPreferences;
    Button btnAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_detail);

        instance = SondageController.getInstance();
        sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);
        projetId = getIntent().getIntExtra("projetId", 0);

        cardResult = findViewById(R.id.card_result);
        cardResult.setVisibility(View.GONE);

        instance.getProjet(projetId).enqueue(new Callback<Projet>() {
            @Override
            public void onResponse(Call<Projet> call, Response<Projet> response) {

                initProjet(response.body());
            }

            @Override
            public void onFailure(Call<Projet> call, Throwable t) {

            }
        });

        initButtons();


        // Add subtitle
        Tools.header(this, SurveyActivity.class, R.string.page_question);

    }

    Dto.ProjetVote projetVote;

    private void initButtons() {

        Button btnResult = findViewById(R.id.btn_result);
        btnResult.setOnClickListener(v -> {
            showResult();
        });

        btnAnswer = findViewById(R.id.btn_answer);
        String json = sharedPreferences.getString(Enum.Settings.CITIZEN.getValue(), "");
        citizen = Tools.getGson().fromJson(json, Citoyen.class);

        // Verify if the user has voted
        instance.canVote(citizen.getId(), this.projetId).enqueue(new Callback<Dto.ProjetVote>() {

            @Override
            public void onResponse(Call<Dto.ProjetVote> call, Response<Dto.ProjetVote> response) {

                projetVote = response.body();
                if (!projetVote.isCanVote())

                    btnAnswer.setEnabled(false);

                else {

                    btnAnswer.setOnClickListener(v -> {
                        showAnswer();
                    });

                }

            }

            @Override
            public void onFailure(Call<Dto.ProjetVote> call, Throwable t) {

            }
        });

    }

    private void initProjet(Projet projet) {
        this.projet = projet;
        TextView txtTitle = findViewById(R.id.txt_title);
        TextView txtQuestion = findViewById(R.id.txt_question);
        txtTitle.setText(this.projet.getTitle());
        txtQuestion.setText(this.projet.get_description());

    }

    /**
     * Show the survey's result
     */
    @SuppressLint("SetTextI18n")
    private void showResult() {


        TextView txtResult = findViewById(R.id.txt_result);
        TextView txtTotal = findViewById(R.id.txt_total);
        TextView txtOui = findViewById(R.id.txt_oui);
        TextView txtNon = findViewById(R.id.txt_non);

        txtResult.setText("Résultat : ");
        txtTotal.setText(String.valueOf(projetVote.getTotalVote()));
        txtOui.setText(String.valueOf(projetVote.getForVote()));
        txtNon.setText(String.valueOf(projetVote.getTotalVote() - projetVote.getForVote()));
        cardResult.setVisibility(View.VISIBLE);


    }

    /**
     * Show the dialog for voting
     */
    private void showAnswer() {

        ConstraintLayout constraintDialogDocument = findViewById(R.id.constraint_dialog_survey);
        View view = LayoutInflater.from(SurveyDetailActivity.this).inflate(R.layout.dialog_survey_answer, constraintDialogDocument);

        AlertDialog.Builder builder = new AlertDialog.Builder(SurveyDetailActivity.this);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();

        view.findViewById(R.id.btn_yes).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Oui Sondage Dialog ******************************************");

            alertDialog.dismiss();
            voting(1);


        });

        view.findViewById(R.id.btn_no).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Non Sondage Dialog ******************************************");

            alertDialog.dismiss();
            voting(0);

        });

        view.findViewById(R.id.btn_cancel).setOnClickListener(view1 -> {

            Log.d("E-Gouvernance Message", "Non Sondage Dialog ******************************************");

            alertDialog.dismiss();

        });

        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        alertDialog.show();

    }

    private void voting(int choice) {
        Sondage sondage = new Sondage(projetId, citizen.getId(), choice);

        String json = Tools.getGson().toJson(sondage);
        instance.vote(sondage).enqueue(new Callback<Sondage>() {
            @Override
            public void onResponse(Call<Sondage> call, Response<Sondage> response) {

                String message = "Oui";
                assert response.body() != null;
                if (response.body().getChoice() == 0)
                    message = "Non";

                projetVote.setTotalVote(projetVote.getTotalVote() + 1);
                projetVote.setForVote(projetVote.getForVote() + response.body().getChoice());
                projetVote.setCanVote(false);

                Toast.makeText(SurveyDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                btnAnswer.setEnabled(false);
            }

            @Override
            public void onFailure(Call<Sondage> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}