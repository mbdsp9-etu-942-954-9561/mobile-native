package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.master.egouvernance.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        findViewById(R.id.btn_connect).setOnClickListener(v->{

            Log.d("E-Gouvernance Message", "Connexion bouton ******************************************");

            Intent intent = new Intent(MainActivity.this, ConnectActivity.class);
            startActivity(intent);

        });

        findViewById(R.id.btn_scan).setOnClickListener(v->{

            Log.d("E-Gouvernance Message", "Scan bouton ******************************************");

            Intent intent = new Intent(MainActivity.this, ScanActivity.class);
            startActivity(intent);

        });

    }

}