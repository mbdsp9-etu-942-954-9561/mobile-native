package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.FamilyController;
import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.model.Membrefamille;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;
import com.master.egouvernance.view.component.FamilyAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Livret de Famille
public class FamilyActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FamilyAdapter familyAdapter;
    private FamilyController instance;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);

        recyclerView = findViewById(R.id.recyclerView);

        instance = FamilyController.getInstance();
        sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);

        initRecyclerView();

        // Add subtitle
        Tools.header(this, HomeActivity.class, R.string.page_family);
    }

    private void initRecyclerView() {

        String json = sharedPreferences.getString(Enum.Settings.CITIZEN.getValue(), "");
        Citoyen citoyen = Tools.getGson().fromJson(json, Citoyen.class);
        instance.livretDeFamille(citoyen.getId()).enqueue(new Callback<ArrayList<Membrefamille>>() {
            @Override
            public void onResponse(Call<ArrayList<Membrefamille>> call, Response<ArrayList<Membrefamille>> response) {

                ArrayList<Membrefamille> membrefamilles = response.body();
                familyAdapter = new FamilyAdapter(FamilyActivity.this, membrefamilles);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FamilyActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(familyAdapter);

            }

            @Override
            public void onFailure(Call<ArrayList<Membrefamille>> call, Throwable t) {

            }
        });


    }
}