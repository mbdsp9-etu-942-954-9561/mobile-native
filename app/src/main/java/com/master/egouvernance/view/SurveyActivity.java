package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.SondageController;
import com.master.egouvernance.model.Projet;
import com.master.egouvernance.utils.Tools;
import com.master.egouvernance.view.component.SurveyAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SondageController instance;
    SurveyAdapter surveyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        recyclerView = findViewById(R.id.recyclerView);
        instance = SondageController.getInstance();

        initRecyclerView();

        // Add subtitle
        Tools.header(this, HomeActivity.class, R.string.page_survey);
    }

    private void initRecyclerView(){

        instance.getProjets().enqueue(new Callback<ArrayList<Projet>>() {
            @Override
            public void onResponse(Call<ArrayList<Projet>> call, Response<ArrayList<Projet>> response) {

                ArrayList<Projet> projets = response.body();
                surveyAdapter = new SurveyAdapter(SurveyActivity.this, projets);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SurveyActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(surveyAdapter);

            }

            @Override
            public void onFailure(Call<ArrayList<Projet>> call, Throwable t) {

            }
        });


    }
}