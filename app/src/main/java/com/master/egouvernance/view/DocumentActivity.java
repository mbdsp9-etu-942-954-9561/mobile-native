package com.master.egouvernance.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.DocumentController;
import com.master.egouvernance.model.DemandeDocument;
import com.master.egouvernance.model.TypeDocument;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;
import com.master.egouvernance.view.component.DocumentAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentActivity extends AppCompatActivity {

    Spinner spinner;
    DocumentAdapter adapter;
    ArrayList<TypeDocument> typeDocuments;
    Button btnDocument;
    TextView txtMessage;
    TypeDocument typeDocument;
    DocumentController instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);

        instance = DocumentController.getInstance();

        spinner = findViewById(R.id.spinner_document);
        btnDocument = findViewById(R.id.btn_demand);
        txtMessage = findViewById(R.id.txt_message);
        txtMessage.setVisibility(View.GONE);

        btnDocument.setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Demande document Dialog ******************************************");
            showConfirmDialog();

        });

        initSpinner();

        // Add subtitle
        Tools.header(this, MemberActivity.class, R.string.page_document);

    }

    /**
     * Initialisation of the typeDocument's list
     */
    private void initSpinner() {

        instance.getDocuments().enqueue(new Callback<ArrayList<TypeDocument>>() {
            @Override
            public void onResponse(Call<ArrayList<TypeDocument>> call, Response<ArrayList<TypeDocument>> response) {

                typeDocuments = response.body();
                adapter = new DocumentAdapter(DocumentActivity.this, typeDocuments);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        Log.d("E-Gouvernance Message", "spinner selectionnée ******************************************");
                        typeDocument = typeDocuments.get(position);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                });

            }

            @Override
            public void onFailure(Call<ArrayList<TypeDocument>> call, Throwable t) {

            }
        });

    }

    /**
     * Show dialog for confirming the document's demand
     */
    private void showConfirmDialog() {

        ConstraintLayout constraintDialogDocument = findViewById(R.id.constraint_dialog_document);
        View view = LayoutInflater.from(DocumentActivity.this).inflate(R.layout.dialog_document, constraintDialogDocument);

        AlertDialog.Builder builder = new AlertDialog.Builder(DocumentActivity.this);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();

        view.findViewById(R.id.btn_confirm).setOnClickListener(v -> {


            SharedPreferences sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);
            int citizenId = sharedPreferences.getInt(Enum.Settings.CITIZEN_ID.getValue(), 0);

            DemandeDocument documentDto = new DemandeDocument(citizenId, typeDocument);
            Log.d("E-Gouvernance Message", "Confirmer document Dialog ******************************************");

            instance.createDocument(documentDto).enqueue(new Callback<DemandeDocument>() {
                @Override
                public void onResponse(Call<DemandeDocument> call, Response<DemandeDocument> response) {
                    if (response.body()!= null){

                        alertDialog.dismiss();

                        Toast.makeText(DocumentActivity.this, "Confimer la demande", Toast.LENGTH_SHORT).show();

                        // Message of confirmation
                        txtMessage.setText(Enum.Settings.DOCUMENT_MESSAGE.getValue());
                        txtMessage.setVisibility(View.VISIBLE);

                    } else {

                        Toast.makeText(DocumentActivity.this, "Erreur dans la demande ", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<DemandeDocument> call, Throwable t) {

                }
            });


        });

        view.findViewById(R.id.btn_cancel).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Annuler document Dialog ******************************************");

            alertDialog.dismiss();
            Toast.makeText(DocumentActivity.this, "Annuler la demande", Toast.LENGTH_SHORT).show();

        });

        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        alertDialog.show();

    }

}