package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.master.egouvernance.R;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;

public class HomeActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);

        findViewById(R.id.btn_family).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Livret de famille bouton ******************************************");

            Intent intent = new Intent(HomeActivity.this, FamilyActivity.class);
            startActivity(intent);

        });

        findViewById(R.id.btn_survey).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Sondage bouton ******************************************");

            Intent intent = new Intent(HomeActivity.this, SurveyActivity.class);
            startActivity(intent);

        });

        findViewById(R.id.btn_logout).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Déconnexion bouton ******************************************");

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear().apply();
            Intent intent = new Intent(HomeActivity.this, MainActivity.class);
            startActivity(intent);
        });

        Tools.header(this, null, R.string.page_main);

    }
}