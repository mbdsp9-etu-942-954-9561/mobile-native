package com.master.egouvernance.view.component;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.master.egouvernance.R;
import com.master.egouvernance.model.TypeDocument;

import java.util.ArrayList;

public class DocumentAdapter extends ArrayAdapter<TypeDocument> {


    public DocumentAdapter(Context context, ArrayList<TypeDocument> typeDocuments) {
        super(context, 0, typeDocuments);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_document, parent, false);

        TextView description = convertView.findViewById(R.id.txt_description);

        TypeDocument currentItem = getItem(position);
        if (currentItem != null)
            description.setText(currentItem.get_description());

        return convertView;
    }
}
