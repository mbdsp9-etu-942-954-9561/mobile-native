package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.SessionController;
import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fiche membre de famille
 */
public class MemberActivity extends AppCompatActivity {

    Button btnDocument;
    int citizenId;
    SessionController instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        instance = SessionController.getInstance();
        SharedPreferences sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);
        citizenId = sharedPreferences.getInt(Enum.Settings.CITIZEN_ID.getValue(), 0);

        initCard();

        btnDocument = findViewById(R.id.btn_document);
        btnDocument.setOnClickListener(v->{

            Log.d("E-Gouvernance Message", "Demande document bouton ******************************************");

            Intent intent = new Intent(MemberActivity.this, DocumentActivity.class);
            // Save the membreId who demands the document
            startActivity(intent);

        });

        Tools.header(this, FamilyActivity.class, R.string.page_member);

    }

    /**
     * Initialization of the member's card
     */
    private void initCard(){

        instance.getCitoyen(citizenId).enqueue(new Callback<Citoyen>() {
            @Override
            public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {

                Citoyen citoyen = response.body();

                TextView txtName = findViewById(R.id.txt_name);
                TextView txtFirstname = findViewById(R.id.txt_firstname);
                TextView txtBirthday = findViewById(R.id.txt_birthday);
                TextView txtLifestatus = findViewById(R.id.txt_lifestatus);

                txtName.setText(citoyen.getName());
                txtFirstname.setText(citoyen.getFirstname());
                txtBirthday.setText(citoyen.getBirthday().toString());
                txtLifestatus.setText(citoyen.getLifeStatus().getLabel());

            }

            @Override
            public void onFailure(Call<Citoyen> call, Throwable t) {

            }
        });



    }
}