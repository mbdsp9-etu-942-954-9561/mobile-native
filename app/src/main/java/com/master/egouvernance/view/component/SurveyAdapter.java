package com.master.egouvernance.view.component;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.master.egouvernance.R;
import com.master.egouvernance.model.Projet;
import com.master.egouvernance.view.SurveyDetailActivity;

import java.util.ArrayList;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.MyHolder> {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Projet> projets;

    public SurveyAdapter(Context context, ArrayList<Projet> projets) {
        this.context = context;
        this.projets = projets;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public SurveyAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_survey, parent, false);
        return new SurveyAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SurveyAdapter.MyHolder holder, int position) {

        Projet projet = this.projets.get(position);
        holder.txtTitle.setText(projet.getTitle());

        holder.link.setOnClickListener(v->{

            Log.d("E-Gouvernance Message", "Sondage lien ******************************************");

            Intent intent = new Intent(context, SurveyDetailActivity.class);
            intent.putExtra("projetId", projet.getId());
            context.startActivity(intent);

        });

    }

    @Override
    public int getItemCount() {
        return this.projets.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView txtTitle;
        TextView link;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_title);
            link = itemView.findViewById(R.id.link);
        }

    }
}