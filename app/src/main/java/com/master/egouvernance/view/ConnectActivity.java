package com.master.egouvernance.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.master.egouvernance.R;
import com.master.egouvernance.controller.SessionController;
import com.master.egouvernance.model.Citoyen;
import com.master.egouvernance.utils.Enum;
import com.master.egouvernance.utils.Tools;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectActivity extends AppCompatActivity {

    private EditText txtPassword;

    private EditText txtEmailAddress;

    private SessionController instance;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        this.txtEmailAddress = findViewById(R.id.txt_email_address);
        this.txtPassword = findViewById(R.id.txt_password);

        instance = SessionController.getInstance();
        sharedPreferences = getSharedPreferences(Enum.Settings.SHARED.getValue(), Context.MODE_PRIVATE);

        connect();

        // Add subtitle
        Tools.header(this, null, R.string.page_connect);

    }


    private void connect() {

        findViewById(R.id.btn_connect).setOnClickListener(v -> {

            Log.d("E-Gouvernance Message", "Se connecter bouton ******************************************");

            String password = txtPassword.getText().toString();
            String emailAddress = txtEmailAddress.getText().toString();

            // Connexion
            instance.signIn(emailAddress, password).enqueue(new Callback<Citoyen>() {
                @Override
                public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {

                    Citoyen citizen = response.body();

                    // Save citizenId
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    String json = Tools.getGson().toJson(citizen);
                    editor.putString(Enum.Settings.CITIZEN.getValue(), json).apply();

                    Intent intent = new Intent(ConnectActivity.this, HomeActivity.class);
                    startActivity(intent);

                }

                @Override
                public void onFailure(Call<Citoyen> call, Throwable t) {
                    t.printStackTrace();
                }
            });



        });

    }


}