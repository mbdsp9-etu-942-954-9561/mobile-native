package com.master.egouvernance.view.component;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.master.egouvernance.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HeaderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeaderFragment extends Fragment {

    private Class<?> aClass;
    private String title;

    public HeaderFragment() {
        // Required empty public constructor
    }

    public static HeaderFragment newInstance(@Nullable Class<?> aClass, String title) {
        HeaderFragment fragment = new HeaderFragment();
        fragment.aClass = aClass;
        fragment.title = title;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_header, container, false);

        TextView title = viewRoot.findViewById(R.id.title);
        title.setText(this.title);

        ImageView previous = viewRoot.findViewById(R.id.imgPrevious);

        if (this.aClass == null) {
            previous.setVisibility(View.GONE);
        } else {
            previous.setOnClickListener(v -> {
                Intent intent = new Intent(viewRoot.getContext(), this.aClass);
                startActivity(intent);
            });
        }


        return viewRoot;
    }
}